#include<iostream>
#include<string>
#include"Vecteur2D.h"

using namespace std;

int main()
{
	cout << "Test unitaire de la classe Vecteur2D" << endl;
	Vecteur2D u1(2, 3);
	Vecteur2D u2(-4, 1), u3;
	cout << "u1 = " << (string)u1 << endl;
	cout << "u2 = " << (string)u2 << endl;
	cout << "u3 = " << (string)u3 << endl;
	u3 = u1 + u2;  //u3 = u1.operator+(u2)
	cout << "u1 = " << (string)u1 << endl;
	cout << "u2 = " << (string)u2 << endl;
	cout << "u3 = " << (string)u3 << endl;
	Vecteur2D u4 = u1 * 3; //u4 = u1.operator*(3)
	cout << "u1 = " << (string)u1 << endl;
	cout << "u4 = " << (string)u4 << endl;
	Vecteur2D u5 = 3.0 * u1; // u5 = 3.0.operator*(u1) : echoue car le type double n'est pas accessible donc le compilateur va tenter autre chose
	//u5 = operator*(3.0 , u1)
	cout << "u1 = " << (string)u1 << endl;
	cout << "u5 = " << (string)u5 << endl;
	Vecteur2D u6 = -u1;
	cout << "u1 = " << (string)u1 << endl;
	cout << "u6 = " << (string)u6 << endl;
	int a = 5, b = 3, c;
	c = a + b;
	//a + b = c;  c'est ABSURDE
	//u1 + u2 = u3; Refuse par le compilateur grace au const a gauche
	Vecteur2D u7 = u1 - u2;
	cout << "u1 = " << (string)u1 << endl;
	cout << "u2 = " << (string)u2 << endl;
	cout << "u7 = " << (string)u7 << endl;

	// Surchager << pour ne plus repeter (string)
	return 0;
}