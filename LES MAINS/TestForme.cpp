#include "Triangle.h"
#include "Cercle.h"
#include "Polygone.h"
#include "Groupe.h"
#include "Erreur.h"
#include "Homoth�tie.h"
#include "Translation.h"
#include "Rotation.h"
#include "Matrice.h"

int main()
{
	
	//Cr�ation d'un SEGMENT
	cout << "////// CREATION D'UN SEGMENT \\\\\\" << endl;

	Vecteur2D p4(12, 8);
	Vecteur2D p5(16, 8);

	cout << "On a cree les points :" << endl;
	cout << "p4" << p4 << endl;
	cout << "p5" << p5 << endl;

	Segment* s = new Segment(p4, p5, "rouge");
	cout << "On a cree le segment, le voici : \n\n" << *s << endl;
	cout << "\nLa longueur du segment : " << s->getLongueur() << endl;
	
	
	//Cr�ation d'un TRIANGLE
	cout << "\n\n////// CREATION D'UN TRIANGLE \\\\\\" << endl;

	Vecteur2D p1(1, 8);
	Vecteur2D p2(4, 10);
	Vecteur2D p3(3, 5);
	double v = p1.getDeterminant(p2);
	cout << "Le determinant : " << v;
	
	cout << "On a cree les points :" << endl;
	cout << "p1" << p1 << endl;
	cout << "p2" << p2 << endl;
	cout << "p3" << p3 << endl;

	Triangle *t = new Triangle(p1, p2, p3, "rouge");
	cout << "On a cree le triangle, le voici : \n\n" << *t << endl;
	cout << "\nL'aire du triangle  : " << t->getAire() << endl;
	
	//Cr�ation d'un CERCLE
	cout << "\n\n////// CREATION D'UN CERCLE \\\\\\" << endl;

	cout << "On a cree le centre du cercle \n" << endl;
	Cercle *c = new Cercle(Segment(Vecteur2D(1,1), Vecteur2D(9,9),"noir"), "bleu");
	cout << *c << endl;

	//Cr�ation d'un polygone (carr�)
	cout << "\n////// CREATION D'UN POLYGONE(CARRE) \\\\\\" << endl;

	Vecteur2D p9(2, 1);
	Vecteur2D p7(2, 2);
	Vecteur2D p8(1, 2);
	Vecteur2D p6(1, 1);

	cout << "On a cree les sommets du carre \n" << endl;
	Polygone * poly = new Polygone("jaune");
	cout << "On affiche le polygone avant l'ajout de segments \n\n" << endl;
	cout << *poly << endl;

	poly->ajouter(p9);
	poly->ajouter(p7);
	poly->ajouter(p8);
	poly->ajouter(p6);

	cout << "\nApres l'ajout de segments : \n\n" << endl;
	cout << *poly << endl;
	cout << "\nAire du polygone : " << poly->getAire() << endl;

	//LAISSER EN COMMENTAIRE
	/*cout << "On retire le le point (2.2) voici maintenant le polygone :" << endl;
	poly->retirer(p7);
	cout << *poly << endl;*/
	
	//Cr�ation d'un groupe
	cout << "\n////// CREATION D'UN GROUPE \\\\\\" << endl;

	cout << "On affiche le groupe avant l'ajout de segments \n\n" << endl;
	Groupe * grp = new Groupe("rose");
	cout << *grp << endl;

	grp->ajouter(s);
	grp->ajouter(t);
	grp->ajouter(c);
	grp->ajouter(poly);

	cout << "\nApres l'ajout des formes \n\n" << endl;
	cout << *grp << endl;
	cout << "\nL'aire du groupe  : " << grp->getAire() << "\n" << endl;


	//Cr�ation d'un second polygone
	cout << "\n////// CREATION D'UN GROUPE(OCTOGONE)\\\\\\" << endl;

	cout << "On affiche le groupe avant l'ajout de segments \n\n" << endl;
	Polygone * ply = new Polygone("turquoise");
	cout << *ply << endl;

	Vecteur2D un(2, 4);
	Vecteur2D deux(3, 4);
	Vecteur2D trois(4, 3);
	Vecteur2D quatre(4, 2);
	Vecteur2D cinq(3, 1);
	Vecteur2D six(2, 1);
	Vecteur2D sept(1, 2);
	Vecteur2D huit(1, 3);

	ply->ajouter(un);
	ply->ajouter(deux);
	ply->ajouter(trois);
	ply->ajouter(quatre);
	ply->ajouter(cinq);
	ply->ajouter(six);
	ply->ajouter(sept);
	ply->ajouter(huit);

	cout << "\nApres l'ajout des fromes \n\n" << endl;
	cout << *ply << endl;
	cout << "\n\nL'aire du groupe  : " << ply->getAire() << "\n" << endl;

	/////////////////////////////////////////////// DESTRUCTION ///////////////////////////////////////////////
	cout << "\n////// DESTRUCTION DES FORMES \\\\\\" << endl;

	//Destruction de l'octogone 
	cout << "On d�truit l'octogone \n" << endl;

	delete(ply);
	//G�n�re une erreur -> OK  cout << *ply << endl; 

	
	//Destruction du groupe
	cout << "On d�truit le groupe \n" << endl;

	delete(grp);
	//G�n�re une erreur -> OK cout << *grp << endl;

	cout << *poly << endl; // Le groupe n'existe plus mais le carr� oui
	

	//Destruction des formes du groupe puis du groupe
	cout << "On retire les formes du groupe, puis on detruit le groupe \n" << endl;
	cout << "On retire les formes du groupe \n" << endl;

	grp->retirer(s);
	grp->retirer(t);
	grp->retirer(c);
	grp->retirer(poly);

	//Le groupe doit �tre vide et les formes intactes
	cout << "On affiche le groupe \n" << *grp << endl;
	cout << "\n On affiche maintenant les formes : \n" << endl;
	cout << *s << endl;
	cout << *t << endl;
	cout << *c << endl;
	cout << *poly << endl;

	//On d�truit maintenant les formes

	cout << "On detruit les formes " << endl;

	cout << "On detruit le segment " << endl;
	delete(s);
	//G�n�re une erreur -> OK cout << *s << endl;

	cout << "On detruit le triangle " << endl;
	delete(t);
	//G�n�re une erreur -> OK cout << *t << endl;

	cout << "On detruit le cercle " << endl;
	delete(c);
	//G�n�re une erreur -> OK cout << *c << endl;

	cout << "On detruit le carre " << endl;
	delete(poly);
	//G�n�re une erreur -> OK cout << *poly << endl;

	cout << "\n On detruit maintenant le groupe " << endl;
	//delete(grp);
	//G�n�re une erreur -> OK out << *grp << endl;
	
/*
	//Cr�ation d'une MATRICE
	cout << "////// CREATION D'UNE MATRICE \\\\\\" << endl;

	Vecteur2D p(12, 8);
	Matrice m = Matrice(3, 2, 5, 4);

	Vecteur2D res;
	res = m * p;
	*/
	return 0;
}