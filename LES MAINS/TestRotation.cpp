#include "Rotation.h"
#include "Polygone.h"

int main()
{
	/////////////////////////////////////// ROTATION ////////////////////////////////////

	Rotation* r = new Rotation(90, Vecteur2D(5, 1));

	//Rotation d'un SEGMENT
	cout << "////// ROTATION D'UN SEGMENT \\\\\\" << endl;

	Segment* sTrans = new Segment(Vecteur2D(4, 2), Vecteur2D(6, 2), "noir");

	cout << "Avant la rotation, voici le segment : \n\n" << *sTrans << endl;
	cout << "\nLa longueur du segment : " << sTrans->getLongueur() << endl;
	sTrans = sTrans->appliquer(r);

	cout << "Apres la rotation, voici le segment : \n\n" << *sTrans << endl;
	cout << "\nLa longueur du segment : " << sTrans->getLongueur() << endl;


	//Rotation d'un CERCLE
	cout << "////// ROTATION D'UN CERCLE \\\\\\" << endl;

	Cercle *cTrans = new Cercle(Segment(Vecteur2D(5, 3), Vecteur2D(6,3), "noir"), "rouge");
	cout << "Avant la rotation:\n\n" << *cTrans << endl;

	cTrans = cTrans->appliquer(r);

	cout << "Apres la rotation :\n\n" << *cTrans << endl;

	//Rotation d'un TRIANGLE
	cout << "////// ROTATION D'UN TRIANGLE \\\\\\" << endl;

	Triangle * tTrans = new Triangle(Vecteur2D(4, 2), Vecteur2D(5, 4), Vecteur2D(6, 2), "noir");
	cout << "Avant la rotation :\n\n" << *tTrans << endl;

	//tTrans = tTrans->appliquer(r); //A commenter pour le test de rotation du groupe

	cout << "Apres la rotation :\n\n" << *tTrans << endl;

	//Rotation d'un POLYGONE
	cout << "////// ROTATION D'UN POLYGONE \\\\\\" << endl;

	Vecteur2D p9(4, 4);
	Vecteur2D p7(6, 4);
	Vecteur2D p8(6, 2);
	Vecteur2D p6(4, 2);

	vector<Vecteur2D> _sommets;
	_sommets.push_back(p9);
	_sommets.push_back(p7);
	_sommets.push_back(p8);
	_sommets.push_back(p6);

	Polygone * pTrans = new Polygone(_sommets, "noir");
	cout << "Avant la rotation :\n\n" << *pTrans << endl;

	//pTrans = pTrans->appliquer(r); //A commenter pour le test de rotation du groupe

	cout << "Apres la rotation :\n\n" << *pTrans << endl;

	//Rotation d'un GROUPE
	cout << "////// ROTATION D'UN GROUPE \\\\\\" << endl;

	Groupe * grpTrans = new Groupe("rose");

	grpTrans->ajouter(pTrans);
	grpTrans->ajouter(tTrans);

	cout << "\nAvant la rotation \n\n" << endl;
	cout << *grpTrans << endl;
	cout << "\nL'aire du groupe  : " << grpTrans->getAire() << "\n" << endl;

	grpTrans = grpTrans->appliquer(r);

	cout << "\nApres la rotation \n\n" << endl;
	cout << *grpTrans << endl;
	cout << "\nL'aire du groupe  : " << grpTrans->getAire() << "\n" << endl;
}