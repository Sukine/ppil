#pragma once
#include "../../Autres/SocketHandler.h"
#include "../../FormesGeometriques/Segment.h"
#include "../../FormesGeometriques/Cercle.h"
#include "../../FormesGeometriques/Triangle.h"
#include "../../FormesGeometriques/Polygone.h"
#include "../../FormesGeometriques/Groupe.h"


class SavingVisitor
{
protected:
	string _name;

public:
	SavingVisitor(string name);
	~SavingVisitor();

	virtual void save(Segment * s) = 0;
	virtual void save(Cercle * c) = 0;
	virtual void save(Triangle * t) = 0;
	virtual void save(Polygone * p) = 0;
	virtual void save(Groupe * g) = 0;

};

