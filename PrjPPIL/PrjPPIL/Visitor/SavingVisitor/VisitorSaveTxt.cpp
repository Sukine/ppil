#include "VisitorSaveTxt.h"

VisitorSaveTxt::VisitorSaveTxt(string name) : SavingVisitor(name)
{
}

void VisitorSaveTxt::save(Segment* s)
{
	FILE* f;
	f = fopen(_name.c_str(), "a+");
	if (f == NULL) {
		perror("fopen");
		cout << "Impossible d'ouvrir le fichier en ecriture ici";
	}
	else
	{
		cout << "On sauvegarde la forme dans le fichier" << endl;
		string str = "S:" + s->getCouleur() + ":" + string(s->getA()) + ":" + string(s->getB()) + "\n";
		const char* tr = _strdup(str.c_str());
		fprintf(f, tr);
		fclose(f);
		free((char*)tr);
	}
}

void VisitorSaveTxt::save(Cercle* c)
{
	FILE* f;
	f = fopen(_name.c_str(), "a+");
	if (f == NULL)
		cout << "Impossible d'ouvrir le fichier en ecriture";
	else
	{
		cout << "On sauvegarde la forme dans le fichier" << endl;
		stringstream ss;

		//Couleur du cercle
		ss << "C:" << c->getCouleur();
		//Serialisation du rayon
		Segment* rayon = new Segment(c->getRayon());
		ss << ":S:" << rayon->getCouleur() << ":" << string(rayon->getA()) << ":" << string(rayon->getB());
		//Ajout de la longueur du rayon
		ss << ":R:" << c->getRayon().getLongueur() << "\n";
		string s = ss.str();

		const char* tr = _strdup(s.c_str());
		fprintf(f, tr);
		fclose(f);
		free((char*)tr);
	}
}

void VisitorSaveTxt::save(Triangle* t)
{
	FILE* f;
	f = fopen(_name.c_str(), "a+");
	if (f == NULL)
		cout << "Impossible d'ouvrir le fichier en ecriture";
	else
	{
		cout << "On sauvegarde la forme dans le fichier" << endl;

		stringstream ss;
		ss << "T:" << t->getCouleur();
		for (unsigned int i = 0; i < 3; i++)
			ss << ":" << t->getLesCotes()[i].getX() << ":" << t->getLesCotes()[i].getY();
		ss << "\n";
		string s = ss.str();
		const char* tr = _strdup(s.c_str());

		fprintf(f, tr);
		fclose(f);
		free((char*)tr);
	}
}

void VisitorSaveTxt::save(Polygone* p)
{
	FILE* f;
	f = fopen(_name.c_str(), "a+");
	if (f == NULL)
		cout << "Impossible d'ouvrir le fichier en ecriture";
	else
	{
		cout << "On sauvegarde la forme dans le fichier" << endl;
		
		stringstream ss;
		ss << "P:" << p->getCouleur() << ":SZ:" << p->getLesSommets().size();
		for (unsigned int i = 0; i < p->getLesSommets().size(); i++)
			ss << ":" << p->getLesSommets()[i].getX() << ":" << p->getLesSommets()[i].getY();
		ss << "\n";
		string s = ss.str();

		const char* tr = _strdup(s.c_str());

		fprintf(f, tr);
		fclose(f);
		free((char*)tr);
	}
}

void VisitorSaveTxt::save(Groupe* g)
{
	FILE* f;
	f = fopen(_name.c_str(), "a+");
	if (f == NULL)
		cout << "Impossible d'ouvrir le fichier en ecriture";
	else
	{
		cout << "On sauvegarde le groupe dans le fichier" << endl;

		//DEBUT_GROUPE
		stringstream ss;

		ss << "G:" << g->getLesFormes().size() << ":" << g->getCouleur() << "\n";
		string s = ss.str();
		const char* tr = _strdup(s.c_str());
		fprintf(f, tr);
		fclose(f);
		free((char*)tr);

		//On envoie les formes une par une
		for (unsigned int i = 0; i < g->getLesFormes().size(); i++) {
			g->getLesFormes()[i]->accept(this);
			f = fopen(_name.c_str(), "a+");
			if (f == NULL)
				cout << "Impossible d'ouvrir le fichier en ecriture";
			else
			{
				if (i == g->getLesFormes().size() - 1) {
					fprintf(f, "FIN_GROUPE\n");
				}
				fclose(f);
			}
		}
	}
}
