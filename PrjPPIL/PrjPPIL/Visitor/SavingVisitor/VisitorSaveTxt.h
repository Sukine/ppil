#pragma once
#include "SavingVisitor.h"


class VisitorSaveTxt : public SavingVisitor
{
public:
	VisitorSaveTxt(string name);

	virtual void save(Segment* s);

	virtual void save(Cercle* c);

	virtual void save(Triangle* t);

	virtual void save(Polygone* p);

	virtual void save(Groupe* g);

};