#include "SavingVisitor.h"

SavingVisitor::SavingVisitor(string name)
{
	FILE* f;
	f = fopen(name.c_str(), "w");
	if (f == NULL)
		cout << "Impossible d'ouvrir le fichier en ecriture";
	else
	{
		cout << "Fichier cree avec succes" << endl;
		_name = name;
		fclose(f);
	}
}

SavingVisitor::~SavingVisitor()
{
}
