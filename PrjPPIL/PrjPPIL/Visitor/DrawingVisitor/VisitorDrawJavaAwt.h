#pragma once
#include "DrawingVisitor.h"

class VisitorDrawJavaAwt : public DrawingVisitor
{
public :
	VisitorDrawJavaAwt(SocketHandler* h);

	virtual void draw(const Segment* s) const throw (Erreur);
	virtual void draw(const Cercle* c) const throw (Erreur);
	virtual void draw(const Triangle* t) const throw (Erreur);
	virtual void draw(const Polygone* p) const throw (Erreur);
	virtual void draw(const Groupe* g) const throw (Erreur);
};

