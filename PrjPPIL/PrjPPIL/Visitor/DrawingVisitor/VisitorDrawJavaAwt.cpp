#include "VisitorDrawJavaAwt.h"

VisitorDrawJavaAwt::VisitorDrawJavaAwt(SocketHandler* h) : DrawingVisitor(h)
{
}

void VisitorDrawJavaAwt::draw(const Segment* s) const throw(Erreur)
{
	if (_h == NULL || s == NULL) {
		throw Erreur("Application impossible : un parametre est null");
	}
	else {
		string str = "S:" + s->getCouleur() + ":" + string(s->getA()) + ":" + string(s->getB()) + "\n";
		const char* tr = _strdup(str.c_str());
		_h->sendSock(tr, strlen(tr));
		free((char*)tr);
	}
}

void VisitorDrawJavaAwt::draw(const Cercle* c) const throw(Erreur)
{
	if (_h == NULL || c == NULL) {
	throw Erreur("Application impossible : un parametre est null");
	}
	else {

		stringstream ss;
		
		//Couleur du cercle
		string s = "C:" + c->getCouleur();
		//Serialisation du rayon
		Segment* rayon = new Segment(c->getRayon());
		s += ":S:" + rayon->getCouleur() + ":" + string(rayon->getA()) + ":" + string(rayon->getB());
		//Ajout de la longueur du rayon
		ss << ":R:" << c->getRayon().getLongueur() << "\n";
		s += ss.str();

		const char* tr = _strdup(s.c_str());

		_h->sendSock(tr, strlen(tr));
		delete(rayon);
		free((char*)tr);
	}
}

void VisitorDrawJavaAwt::draw(const Triangle* t) const throw(Erreur)
{
	if (_h == NULL || t == NULL) {
			throw Erreur("Application impossible : un parametre est null");
	}
	else {
		stringstream ss;
		ss << "T:" << t->getCouleur();
		for (unsigned int i = 0; i < 3; i++)
			ss << ":" << t->getLesCotes()[i].getX() << ":" << t->getLesCotes()[i].getY();
		ss << "\n";
		string s = ss.str();
		const char* tr = _strdup(s.c_str());
		_h->sendSock(tr, strlen(tr));
		free((char*)tr);
	}
}

void VisitorDrawJavaAwt::draw(const Polygone* p) const throw(Erreur)
{
	if (_h == NULL || p == NULL) {
		throw Erreur("Application impossible : un parametre est null");
	}
	else {

		stringstream ss;
		ss << "P:" << p->getCouleur() << ":SZ:" << p->getLesSommets().size();
		for (unsigned int i = 0; i < p->getLesSommets().size(); i++)
			ss << ":" << p->getLesSommets()[i].getX() << ":" << p->getLesSommets()[i].getY();
		ss << "\n";
		string s = ss.str();

		const char* tr = _strdup(s.c_str());
		_h->sendSock(tr, strlen(tr));
		free((char*)tr);
	}

}

void VisitorDrawJavaAwt::draw(const Groupe* g) const throw(Erreur)
{
	if (_h == NULL || g == NULL) {
		throw Erreur("Application impossible : un parametre est null");
	}
	else {

		_h->sendSock("DEBUT_GROUPE\n", 13);
		//DEBUT_GROUPE
		//Il relit une deuxi�me fois avant la boucle while
		//TAILLE:#forme1#forme2.......

		//On envoie les formes une par une
		for (unsigned int i = 0; i < g->getLesFormes().size(); i++)
			//draw(g->getLesFormes()[i]);
			g->getLesFormes()[i]->accept(this);

		//draw(FormeGeo *f)
		
		//On indique la fin
		_h->sendSock("FIN_GROUPE\n", 11);
	}
}
