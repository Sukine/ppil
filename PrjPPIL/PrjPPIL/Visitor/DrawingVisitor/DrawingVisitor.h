#pragma once
#include "../../Autres/SocketHandler.h"
#include "../../FormesGeometriques/Segment.h"
#include "../../FormesGeometriques/Cercle.h"
#include "../../FormesGeometriques/Triangle.h"
#include "../../FormesGeometriques/Polygone.h"
#include "../../FormesGeometriques/Groupe.h"

class DrawingVisitor
{
protected :

	SocketHandler *_h;

public :
	DrawingVisitor(SocketHandler* h);
	~DrawingVisitor();

	virtual void draw(const Segment * s) const throw (Erreur)= 0;
	virtual void draw(const Cercle * c) const throw (Erreur)= 0;
	virtual void draw(const Triangle * t) const throw (Erreur)= 0;
	virtual void draw(const Polygone * p) const throw (Erreur)= 0;
	virtual void draw(const Groupe * g) const throw (Erreur)= 0;

};

