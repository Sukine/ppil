#pragma once
#include "Transformation.h"

class Translation : public Transformation
{
private:
	Vecteur2D _vecteur;
public:

	Translation(const Vecteur2D v);
	~Translation();

	inline const Vecteur2D getVecteur() const { return _vecteur;};

	virtual const Vecteur2D transformer(const Vecteur2D &v) const;
};


