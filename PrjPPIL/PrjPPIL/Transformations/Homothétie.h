#pragma once
#include "Transformation.h"
#include "../FormesGeometriques/Vecteur2D.h"

class Homothétie : public Transformation
{

private:
	double _zoom;
	Vecteur2D _centre;
public:

	Homothétie(const double z, Vecteur2D c);
	~Homothétie();
	const double getZoom() const { return _zoom; };
	const Vecteur2D getCentre() const { return _centre; };

	virtual const Vecteur2D transformer(const Vecteur2D &v) const;
};

