#include "Homothétie.h"


Homothétie::Homothétie(const double z, Vecteur2D c) : _zoom(z), _centre(c)
{
}

Homothétie::~Homothétie()
{
}

const Vecteur2D Homothétie::transformer(const Vecteur2D & v) const
{ 
	return _zoom * (v - _centre) + _centre;
}
