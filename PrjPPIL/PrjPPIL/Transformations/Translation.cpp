#include "Translation.h"

Translation::Translation(const Vecteur2D v) : _vecteur(v)
{
}

Translation::~Translation()
{
}

const Vecteur2D Translation::transformer(const Vecteur2D & v) const
{
	return _vecteur + v;
}
