#pragma once
#include "Transformation.h"
#include "../Autres/Matrice.h"
#include "../FormesGeometriques/Vecteur2D.h"

class Rotation : public Transformation
{
private:
	double _angle; // en radian
	Vecteur2D _centre;
	Matrice _m;

public:

	Rotation(const double a, Vecteur2D c);
	~Rotation();
	inline const double getAngle() const {	return _angle;	};
	inline const Vecteur2D getCentre() const { return _centre; };
	inline const Matrice getMatrice() const { return _m; };

	virtual const Vecteur2D transformer(const Vecteur2D &v) const; 
};

