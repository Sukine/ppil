#include "Rotation.h"
#include <cmath>

using namespace std;

Rotation::Rotation(double a, Vecteur2D c)
{
	_angle = a;
	_centre = c;
	
	double cosi = cos(_angle);
	Vecteur2D v1(cosi, -sin(_angle));
	Vecteur2D v2(sin(_angle), cosi);
	_m = Matrice(v1,v2);
}

Rotation::~Rotation()
{
}

const Vecteur2D Rotation::transformer(const Vecteur2D &v) const
{
	return (_m * (v - _centre) + _centre);
}

