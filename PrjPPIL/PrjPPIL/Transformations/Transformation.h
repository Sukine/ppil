#pragma once
#include "../FormesGeometriques/Vecteur2D.h"
#include "../FormesGeometriques/FormeSimple.h"
#include "../FormesGeometriques/Groupe.h"
#include "../FormesGeometriques/Triangle.h"
#include "../FormesGeometriques/Cercle.h"

class Transformation
{

public:

	Transformation();
	~Transformation();

	virtual const Vecteur2D transformer(const Vecteur2D &v) const = 0;

};