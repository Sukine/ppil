#include "SocketHandler.h"

SocketHandler::SocketHandler(SOCKET s, SOCKADDR_IN a) : _sock(s), _sin(a)
{
}

SocketHandler::SocketHandler(const SocketHandler& s) : _sock(s.getSocket()), _sin(s.getSin())
{
}

SocketHandler::~SocketHandler()
{
}

void SocketHandler::initSock()
{
	inet_pton(AF_INET, "127.0.0.1", &_sin.sin_addr.s_addr);
	if (connect(_sock, (SOCKADDR*)&_sin, sizeof(_sin)) < 0) { cout << "Impossible de se connecter"; exit(1); }
}

void SocketHandler::sendSock(const char* msg, int len) const throw(Erreur)
{
	if (strcmp(msg, "") == 0)
		throw Erreur("Envoi impossible : msg est vide");
	else if (len <= 0) 
		throw Erreur("Envoi impossible : longueur du msg");
	else 
		if (send(_sock, msg, len, 0) < 0) { cerr << "Erreur lors de l'envoi"; exit(1); }
}
