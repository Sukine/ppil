#pragma once
#include "../FormesGeometriques/Vecteur2D.h"

class Matrice
{
	Vecteur2D _v1;
	Vecteur2D _v2;

public :
	Matrice();
	Matrice(const Vecteur2D v1, const Vecteur2D v2);
	Matrice(const double a, const double b, const double c, const double d);
	Matrice(const Matrice &m);
	~Matrice();

	inline const Vecteur2D getV1() const { return _v1; };
	inline const Vecteur2D getV2() const { return _v2; };

	const Vecteur2D operator * (const Vecteur2D &v) const;
	operator string() const;
};

