#pragma once

using namespace std;

#include <string>
#include <iostream>

class Erreur :public exception 
{
public:
	const static int LONGUEURMESSAGE = 50;
	char message[1 + LONGUEURMESSAGE];
	Erreur();
	Erreur(const char * messageErreur);

	operator string() const;
};

ostream & operator << (ostream & os, const Erreur & erreur);



