#pragma once
#include <WinSock2.h>
#include"Singleton.h"
#include "Erreur.h"

class Winsock : public Singleton<Winsock>
{
	friend class Singleton<Winsock>;

private:
	// Constructeur/destructeur
	Winsock() 
	{
		int r;
		WSADATA wsaData;
		r = WSAStartup(MAKEWORD(2, 0), &wsaData);
		if (r != 0)
			throw Erreur("L'initialisation a echoue");
	}
	~Winsock() 
	{
		WSACleanup();
		cout << "Liberation de la winsock OK" << endl;
	}

public:
	// Interface publique
	WSADATA getValue() { return _wsaData; }

private:
	// Variable membre
	WSADATA _wsaData;

};