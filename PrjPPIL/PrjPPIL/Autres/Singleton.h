#include<winsock2.h>
#include<iostream>
#pragma once

using namespace std;

template <typename T>
class Singleton
{
    protected:
    
        Singleton() { }
        ~Singleton() { cout << "Destruction du singleton" << endl; }

    public:
        static T* getInstance()
        {
            if (NULL == _singleton)
            {
                cout << "Creation du singleton." << endl;
                _singleton = new T;
            }
            else
                cout << "Le singleton existe deja!" << endl;
            return (static_cast<T*> (_singleton));
        }

        static void kill()
        {
            if (NULL != _singleton)
            {
                delete _singleton;
                _singleton = NULL;
            }
        }

    private:
        // Unique instance
        static T* _singleton;
};

template <typename T>
T* Singleton<T>::_singleton = NULL;