#include "Matrice.h"
#include <sstream>

Matrice::Matrice()
{
}

Matrice::Matrice(const Vecteur2D v1, const Vecteur2D v2)
{
	_v1 = v1;
	_v2 = v2;
}

Matrice::Matrice(const double a, const double b, const double c, const double d)
{
	_v1 = Vecteur2D(a, b);
	_v2 = Vecteur2D(c, d);
} 

Matrice::Matrice(const Matrice & m)
{
	_v1 = m._v1;
	_v2 = m._v2;
}

Matrice::~Matrice()
{
}

const Vecteur2D Matrice::operator*(const Vecteur2D & v) const
{
	return Vecteur2D(_v1 * v, _v2 * v);
}

Matrice::operator string() const
{
	ostringstream os;
	os << "(" << _v1 << ")\n";
	os << "(" << _v2 << ")";
	return os.str();
}


