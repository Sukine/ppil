#pragma once
#include "Singleton.h"
#include "Erreur.h"
#include <sstream>
#include <WS2tcpip.h>
#include "Winsock.h"
#pragma comment(lib, "ws2_32.lib")

using namespace std;

class SocketHandler
{
	SOCKET _sock;
	SOCKADDR_IN _sin;

public : 
	
	SocketHandler(SOCKET s, SOCKADDR_IN a);
	SocketHandler(const SocketHandler & s);

	~SocketHandler();

	inline const SOCKET getSocket() const { return _sock; }
	inline const SOCKADDR_IN getSin() const { return _sin; }
	inline void setSocket(SOCKET s) { _sock = s; }
	inline void setSin(SOCKADDR_IN s) { _sin = s; }

	void initSock();
	void sendSock(const char * msg, int len) const;

};


