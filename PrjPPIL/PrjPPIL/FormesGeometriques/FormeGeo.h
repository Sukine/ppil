#pragma once

#include <string>
#include "Vecteur2D.h"
#include <sstream>
#include "../Autres/Erreur.h"

class SavingVisitor;
class DrawingVisitor;
class SocketHandler;
class Transformation;
class Groupe;


using namespace std;
class FormeGeo
{
protected:
	string _couleur;
	Groupe* _appartient = NULL;

public:

	FormeGeo(string clr) throw (Erreur);

	inline const Groupe* getGrp() const { return _appartient; };
	inline void setGrp(Groupe* grp) { _appartient = grp; };
	inline const string getCouleur() const { return _couleur; }
	inline void setCouleur(const string c) { _couleur = c; }

	virtual const double getAire() const = 0;
	virtual FormeGeo* appliquer(const Transformation *t) const throw (Erreur)  = 0;

	virtual void accept(SavingVisitor *visitor) = 0;
	virtual void accept(const DrawingVisitor *visitor) const = 0;


	virtual ostream & afficher(ostream&os) const;
	friend ostream&	operator<<(ostream&, const FormeGeo &f);
	virtual operator string() const;

	inline bool operator==(FormeGeo const& f) const { return (_couleur.compare(f.getCouleur()) == 0); };

};

