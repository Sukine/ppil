#include "Polygone.h"
#include "Triangle.h"
#include "../Transformations/Transformation.h"
#include "../Autres/SocketHandler.h"
#include "../Autres/Erreur.h"
#include "../Visitor/DrawingVisitor/DrawingVisitor.h"
#include "../Visitor/SavingVisitor/SavingVisitor.h"

Polygone::Polygone(string couleur) : FormeGeo(couleur)
{
}

Polygone::Polygone(vector<Vecteur2D> cotes, string couleur) : FormeGeo(couleur), _pSommets(cotes)
{
}

Polygone::~Polygone()
{
}

void Polygone::ajouter(Vecteur2D s)
{
	_pSommets.push_back(s);
}

void Polygone::retirer(Vecteur2D s)
{
	for (vector <Vecteur2D>::iterator it = _pSommets.begin(); it != _pSommets.end(); it++) {
		cout << "On compare " << s << " et " << *it << endl;
		if (s == *it) {
			//delete(it);
			_pSommets.erase(it);

			return;
		}
	}
}

const double Polygone::getAire() const
{
	double aire = 0;
	vector<Triangle *> tri;
	tri = decompositionTriangle();
	for (unsigned int i = 0; i < tri.size(); i++) {
		aire += tri[i]->getAire();
	}
	return aire;
}

FormeGeo* Polygone::appliquer(const Transformation* t) const throw(Erreur)
{
	if (t == NULL) {
		throw Erreur("Application impossible : t est null");
	}
	else {
		vector<Vecteur2D> _nvSommets;
		for (unsigned int i = 0; i < _pSommets.size(); i++) {
			_nvSommets.push_back(t->transformer(_pSommets[i]));
		}
		return new Polygone(_nvSommets, _couleur);
	}
}


vector<Triangle*> Polygone::decompositionTriangle() const
{
	vector <Triangle *> decompo;

	for (unsigned int i = 0; i < _pSommets.size() - 2; i++) {
		cout << "\nOn cree le triangle numero " << i << endl;
		// On effectue un cast sur i pour �viter l'overflow lors de l'addition
		Triangle * t = new Triangle(_pSommets[0], _pSommets[(double)i+1], _pSommets[(double)i+2], "noir"); 
		cout << "On vient de creer le triangle numero " << i << "Le voici : " << endl;
		cout << *t << endl;
		decompo.push_back(t);
	}
	return decompo;
}

void Polygone::accept(SavingVisitor* visitor)
{
	{ visitor->save(this); }
}

void Polygone::accept(const DrawingVisitor* visitor)const
{
	{ visitor->draw(this); }
}

ostream & Polygone::afficher(ostream & os) const
{
	os << "POLYGONE : \n";
	FormeGeo::afficher(os);
	os << "Ce polygone comporte " << _pSommets.size() << " cotes. \n";
	for (unsigned int i = 0; i < _pSommets.size(); i++) {
		os << _pSommets[i] << "\n";
	}
	return os;
}

Polygone::operator string() const
{
	ostringstream os;
	os << "POLYGONE : \n" << " Couleur : " << FormeGeo::operator std::string() << "\n Ce poylgone comporte " << _pSommets.size() << " cotes. \n";
	for (unsigned int i = 0; i < _pSommets.size(); i++) {
		os << string(_pSommets[i]) << "\n";
	}
	return os.str();
}

bool Polygone::operator==(const Polygone & p) const
{
	if (FormeGeo::operator==(p) == false)
		return false;
	else {
		if (p.getLesSommets().size() != _pSommets.size()) return false;
		else {
			for (unsigned int i = 0; i < _pSommets.size(); i++) {
				if (!(_pSommets[i] == p.getLesSommets()[i])) return false;
			}
			return true;
		}
	}

}

ostream & operator<<(ostream & os, const Polygone & p)
{
	return p.afficher(os);
}