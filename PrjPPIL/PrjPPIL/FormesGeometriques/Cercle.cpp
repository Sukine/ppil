#include "Cercle.h"
#include "../Transformations/Transformation.h"
#include "../Autres/SocketHandler.h"
#include "../Visitor/SavingVisitor/SavingVisitor.h"
#include "../Visitor/DrawingVisitor/DrawingVisitor.h"

#define PI 3.14159

Cercle::Cercle(Segment r, string couleur) : FormeGeo(couleur), _rayon(r)
{
}

Cercle::Cercle(const Cercle & c) : FormeGeo(c._couleur), _rayon(c._rayon)
{
}

Cercle::~Cercle()
{
}

const double Cercle::getAire() const
{
	return 2 * PI*_rayon.getLongueur();
}

FormeGeo* Cercle::appliquer(const Transformation* t) const throw(Erreur)
{
	if (t == NULL) {
		throw Erreur("Application impossible : t est null");
	}
	else 
		return new Cercle(Segment(t->transformer(_rayon.getA()), t->transformer(_rayon.getB()), _couleur) , _couleur);
}

void Cercle::accept(SavingVisitor* visitor)
{
	visitor->save(this); 
}

void Cercle::accept(const DrawingVisitor* visitor) const
{
	visitor->draw(this); 
}

ostream & Cercle::afficher(ostream & os) const
{
	os << "CERCLE : \n";
	FormeGeo::afficher(os);
	os << " Rayon : " << _rayon << " | Centre : " << getCentre() << endl;
	return os;
}

Cercle::operator string() const
{
	ostringstream os;
	os << "CERCLE : \n" << " Couleur : " << FormeGeo::operator std::string() << "\n Rayon : " << string(_rayon) << " | Centre : " << string(getCentre()) << endl;;
	return os.str();
}

bool Cercle::operator==(Cercle const & c) const
{
	if (FormeGeo::operator==(c) == false)
		return false;
	else
		return (_rayon == c.getRayon());
}

ostream & operator<<(ostream & os, const Cercle & c)
{
	return c.afficher(os);
}
