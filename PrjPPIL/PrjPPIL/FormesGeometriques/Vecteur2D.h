#pragma once
#include <string>
#include <iostream>

using namespace std;

template <class T>
inline const T operator - (const T & u, const T & v)
{
	return u + -v;
}

class Vecteur2D
{
public:
	double x, y;

	explicit Vecteur2D(const double & x = 0, const double & y = 0);
	~Vecteur2D();

	inline const double getX() const { return x; }
	inline const double getY() const { return y; }

	/**Calcule la longueur du segment qui commence � this et finit � B*/
	const double getLongueur(Vecteur2D B) const;

	const Vecteur2D operator + (const Vecteur2D & u) const { return Vecteur2D(x + u.x, y + u.y); }
	const Vecteur2D operator * (const double & a) const { return Vecteur2D(x * a, y * a); }

	const double operator * (const Vecteur2D & u) const { return u.x*this->x + u.y*this->y; }

	const Vecteur2D operator - () const { return Vecteur2D(-x, -y); }

	const double norme() const { return sqrt(*this * *this); }

	operator string() const;

	bool operator==(Vecteur2D const& v) const;

	friend ostream& operator<<(ostream& os, const Vecteur2D& v);

	const double getDeterminant(const Vecteur2D& u) const { return (double)abs((x * u.y) - (u.x * y)); }

}; 

inline const Vecteur2D operator * (const double& a, const Vecteur2D &v) { return v * a; }

