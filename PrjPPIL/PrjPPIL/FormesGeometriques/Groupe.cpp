#include "Groupe.h"
#include "../Transformations/Transformation.h"
#include "../Autres/SocketHandler.h"
#include "../Visitor/DrawingVisitor/DrawingVisitor.h"
#include "../Visitor/SavingVisitor/SavingVisitor.h"

Groupe::Groupe(string couleur) : FormeGeo(couleur)
{
}

Groupe::Groupe(vector<FormeGeo*> formes, string Couleur) throw(Erreur) : FormeGeo(Couleur)
{
	try {
		if (formes.size() == 0)
			throw Erreur("Construction impossible : vector vide");
		else
			_formes = formes;
	}
	catch (Erreur e) {
		cerr << e << endl;
	}
}

Groupe::Groupe(const Groupe & g) : FormeGeo(g.getCouleur())
{
	for (unsigned int i = 0; i < g.getLesFormes().size(); i++)
		_formes.push_back(g.getLesFormes()[i]);
}

Groupe::~Groupe()
{

}

void Groupe::ajouter(FormeGeo* f) throw (Erreur)
{
	if (f->getGrp() == NULL)
	{
		f->setCouleur(getCouleur());
		_formes.push_back(f);
		f->setGrp(this);
	}
	else
		throw Erreur("ECHEC: la forme appartient deja a un groupe");
}

void Groupe::retirer(FormeGeo * f)
{
	for (vector <FormeGeo *>::iterator it = _formes.begin(); it != _formes.end(); it++) {
		cout << "On compare " << *f << " et " << **it << endl;
		if (*f == **it) {
			_formes.erase(it);

			return;
		}
	}
}

const double Groupe::getAire() const
{
	double ttlAire = 0;
	for (unsigned int i = 0; i < _formes.size(); i++) {
		ttlAire += _formes[i]->getAire();
	}
	return ttlAire;
}

FormeGeo* Groupe::appliquer(const Transformation* t) const throw(Erreur)
{
	if (t == NULL) {
		throw Erreur("Application impossible : t est null");
	}
	else {
		vector<FormeGeo*> _actuelFormes = getLesFormes();

		vector<FormeGeo*> _nvFormes;
		for (vector <FormeGeo*>::iterator it = _actuelFormes.begin(); it != _formes.end(); it++) {
			_nvFormes.push_back((*it)->appliquer(t));
		}

		return new Groupe(_nvFormes, _couleur);
	}

}

void Groupe::accept(SavingVisitor* visitor)
{
	{ visitor->save(this); }
}

void Groupe::accept(const DrawingVisitor* visitor)const
{
	{ visitor->draw(this); }
}

ostream & Groupe::afficher(ostream &os) const
{
	os << "GROUPE ::\n";
	FormeGeo::afficher(os);
	os << "Ce groupe a " << _formes.size() << " formes. \n" << "Liste des formes : \n";
	for (unsigned int i = 0; i < _formes.size(); i++) {
		os << *_formes[i] << "\n";
	}
	os << endl;
	return os;
}

Groupe::operator string() const
{
	ostringstream os;
	os << "GROUPE : \n" << " Couleur : " << FormeGeo::operator std::string() << "\n Ce groupe comporte " << _formes.size() << " formes. \n Listes des formes : \n";
	for (unsigned int i = 0; i < _formes.size(); i++) {
		os << string(*_formes[i]) << "\n";
	}
	os << endl;
	return os.str();
}

bool Groupe::operator==(const Groupe & g) const
{
	if (FormeGeo::operator==(g) == false)
		return false;
	else {
		if (_formes.size() != g.getLesFormes().size()) return false;
		else {
			for (unsigned int i = 0; i < _formes.size(); i++) {
				if (!(_formes[i] == g.getLesFormes()[i])) return false;
			}
			return true;
		}
	}
}

ostream & operator<<(ostream &os, const Groupe & g)
{
	return g.afficher(os);
}
