#pragma once
#include "FormeGeo.h"
#include "Segment.h"

class Transformation;
class SocketHandler;
class SavingVisitor;
class DrawingVisitor;

class Triangle : public FormeGeo
{
	Vecteur2D _sommets[3];

public:

	Triangle(Vecteur2D A, Vecteur2D  B, Vecteur2D C, string couleur);
	Triangle(const Triangle &t);
	~Triangle();

	inline const Vecteur2D * getLesCotes() const { return _sommets; };
	inline const Vecteur2D getA() const { return _sommets[0]; };
	inline const Vecteur2D getB() const { return _sommets[1]; };
	inline const Vecteur2D getC() const { return _sommets[2]; };

	//virtual const Vecteur2D getCentre() const;
	virtual const double getAire() const;
	//virtual void dessinerUneForme(SocketHandler* h) throw (Erreur);
	//virtual string getTrame() const;
	virtual FormeGeo* appliquer(const Transformation* t) const throw (Erreur);

	virtual void accept(SavingVisitor* visitor);
	virtual void accept(const DrawingVisitor* visitor)const;


	virtual ostream & afficher(ostream&os) const;
	friend ostream&	operator<<(ostream&, const Triangle &t);
	virtual operator string() const;
	bool operator == (const Triangle &t) const;


};

