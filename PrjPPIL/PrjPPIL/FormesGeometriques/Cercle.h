#pragma once
#include "FormeGeo.h"
#include "Segment.h"

class Transformation;
class SocketHandler;
class SavingVisitor;
class DrawingVisitor;

class Cercle : public FormeGeo
{
	Segment _rayon;

public:
	Cercle(Segment r, string couleur);
	Cercle(const Cercle &c);
	~Cercle();

	inline const Segment getRayon() const { return _rayon; };

	inline const Vecteur2D getCentre() const { return _rayon.getA(); };
	virtual const double getAire() const;
	virtual FormeGeo* appliquer(const Transformation* t) const throw (Erreur);

	virtual void accept(SavingVisitor* visitor);
	virtual void accept(const DrawingVisitor* visitor)const;

	virtual ostream & afficher(ostream & os) const;
	friend ostream&	operator<<(ostream& os, const Cercle &c);
	virtual operator string() const ;
	bool operator==(Cercle const &c) const;
};

