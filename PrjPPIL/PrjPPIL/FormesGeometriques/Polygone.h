#pragma once
#include "FormeGeo.h"
#include "Segment.h"
#include <vector>

class Transformation;
class Triangle;
class SocketHandler;
class SavingVisitor;
class DrawingVisitor;

class Polygone : public FormeGeo
{
	vector <Vecteur2D> _pSommets;

public:

	Polygone(string couleur);
	Polygone(vector <Vecteur2D> cotes, string couleur);
	~Polygone();

public:
	inline const vector<Vecteur2D> getLesSommets() const { return _pSommets; }

	void ajouter(Vecteur2D  s);
	void retirer(Vecteur2D  s);

	virtual const double getAire() const;
	virtual FormeGeo* appliquer(const Transformation* t) const throw (Erreur);

	vector<Triangle *> decompositionTriangle() const;

	virtual void accept(SavingVisitor* visitor);
	virtual void accept(const DrawingVisitor* visitor)const;


	const Vecteur2D operator [] (int i) const { return _pSommets[i]; }
	Vecteur2D operator [] (int i) { return _pSommets[i]; }

	virtual ostream & afficher(ostream & os) const;
	friend ostream&	operator<<(ostream& os, const Polygone &p);
	virtual operator string() const;
	bool operator == (const Polygone &p) const;
};




