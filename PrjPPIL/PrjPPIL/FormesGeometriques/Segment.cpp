#include "Segment.h"
#include <sstream>
#include "../Transformations/Transformation.h"
#include "../Autres/SocketHandler.h"
#include "../Visitor/DrawingVisitor/DrawingVisitor.h"
#include "../Visitor/SavingVisitor/SavingVisitor.h"


Segment::Segment(Vecteur2D a, Vecteur2D b, string couleur) : FormeGeo(couleur)
{
	_a = a;
	_b = b;
}

Segment::Segment(const Segment & s) : FormeGeo(s.getCouleur())
{
	_a = s._a;
	_b = s._b;
}

Segment::~Segment()
{
}

const double Segment::getAire() const
{
	return 0;
}

const double Segment::getLongueur() const
{
	return _a.getLongueur(_b);
}

FormeGeo * Segment::appliquer(const Transformation *t) const throw(Erreur)
{
	if (t == NULL) {
		throw Erreur("Application impossible : t est null");
	}
	else 
		return new Segment(t->transformer(_a), t->transformer(_b), _couleur);
}

void Segment::accept(SavingVisitor* visitor)
{
	visitor->save(this);
}

void Segment::accept(const DrawingVisitor* visitor)const
{
	 visitor->draw(this);
}


ostream & Segment::afficher(ostream & os) const
{
	os << "SEGMENT : \n";
	FormeGeo::afficher(os);
	os << " Point A : " << string(_a) << "\n Point B : " << string(_b);
	return os;
}

Segment::operator string() const
{
	ostringstream os;
	os << "SEGMENT : \n" << " Couleur : " << FormeGeo::operator std::string() << "\n Point A : " << string(_a) << "\n Point B : " << string(_b);
	return os.str();
}

bool Segment::operator==(Segment const & s) const
{
	if (FormeGeo::operator==(s) == false)
		return false;
	else
		return (_a == s.getA() && _b == s.getB());
}

ostream& operator << (ostream& os, const Segment& s)
{
	return s.afficher(os);
}
