#include "FormeGeo.h"
#include <iostream>
#include <sstream>
#include "../Autres/Erreur.h"
#include "../Visitor/DrawingVisitor/DrawingVisitor.h"*
#include "../Visitor/SavingVisitor/SavingVisitor.h"

using namespace std;

FormeGeo::FormeGeo(string clr) throw (Erreur)
{
	try {
		if (strcmp(clr.c_str(), "") == 0)
			throw Erreur("Construction impossible : couleur vide");
		else
			_couleur = clr;
	}
	catch (Erreur e) {
		cerr << e << endl;
	}
}

ostream & FormeGeo::afficher(ostream &os) const
{
	os << " Couleur : " << _couleur << endl;
	return os;
}

FormeGeo::operator string() const
{
	ostringstream os;
	os << _couleur;
	return os.str();
}

ostream & operator<<(ostream &os, const FormeGeo & f)
{
	return f.afficher(os);
}

