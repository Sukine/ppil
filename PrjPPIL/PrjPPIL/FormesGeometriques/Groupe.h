#pragma once
#include "FormeGeo.h"
#include <vector>

class Transformation;
class SocketHandler;
class SavingVisitor;
class DrawingVisitor;

class Groupe : public FormeGeo
{
	vector <FormeGeo *> _formes;

public:
	Groupe(string couleur) throw (Erreur);
	Groupe(vector<FormeGeo*> formes, string couleur) throw(Erreur);
	Groupe(const Groupe &g);
	~Groupe();

	inline const vector<FormeGeo*> getLesFormes() const { return _formes; }

	void ajouter(FormeGeo * f) throw (Erreur);
	void retirer(FormeGeo * f);

	virtual const double getAire() const;
	virtual FormeGeo* appliquer(const Transformation* t) const throw (Erreur);

	virtual void accept(SavingVisitor* visitor);
	virtual void accept(const DrawingVisitor* visitor) const;

	const FormeGeo* operator [] (int i) const { return _formes[i]; };
	FormeGeo* operator [] (int i) { return _formes[i]; };

	virtual ostream & afficher(ostream&os) const;
	friend ostream&	operator<<(ostream&, const Groupe &g);
	virtual operator string() const;

	bool operator == (const Groupe &g) const;
};

