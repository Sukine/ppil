#include "Vecteur2D.h"
#include <sstream>
#include "../Autres/Erreur.h"

//------------ implémentation des fonctions inline ----------------------

Vecteur2D::Vecteur2D(const double & x, const double & y) : x(x), y(y) {}

Vecteur2D::~Vecteur2D()
{
}

const double Vecteur2D::getLongueur(Vecteur2D B) const
{

	Vecteur2D tmp = B - *this;
	return abs(tmp.norme());
}

Vecteur2D::operator string() const
{
	ostringstream os;
	os << x << ":" << y;
	return os.str();
}

ostream & operator << (ostream & os, const Vecteur2D & u)
{
	os << string(u);
	return os;
}

bool Vecteur2D:: operator==(Vecteur2D const& v) const
{
	return(x == v.x && y == v.y);
}