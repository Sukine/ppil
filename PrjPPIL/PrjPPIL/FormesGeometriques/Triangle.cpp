#include "Triangle.h"
#include "../Transformations/Transformation.h"
#include "../Autres/SocketHandler.h"
#include "../Visitor/DrawingVisitor/DrawingVisitor.h"
#include "../Visitor/SavingVisitor/SavingVisitor.h"

Triangle::Triangle(Vecteur2D A, Vecteur2D B, Vecteur2D C, string couleur) : FormeGeo(couleur)
{
		_sommets[0] = A;
		_sommets[1] = B;
		_sommets[2] = C;
}

Triangle::Triangle(const Triangle & t) : FormeGeo(t.getCouleur())
{
	_sommets[0] = t._sommets[0];
	_sommets[1] = t._sommets[1];
	_sommets[2] = t._sommets[2];
}

Triangle::~Triangle()
{
}

const double Triangle::getAire() const
{
	Vecteur2D a = _sommets[0];
	Vecteur2D b = _sommets[1];
	Vecteur2D c = _sommets[2];
	//Aire = |det(ab,ac)|/2
	Vecteur2D u = b - a; //car u = AB
	Vecteur2D v = c - a; //car v = AC
	return(u.getDeterminant(v) / 2); //getDeterminant renvoie la valeur absolue 
}

FormeGeo* Triangle::appliquer(const Transformation* t) const throw(Erreur)
{
	if (t == NULL) {
		throw Erreur("Application impossible : t est null");
	}
	else {
		return new Triangle(t->transformer(_sommets[0]), t->transformer(_sommets[1]), t->transformer(_sommets[2]), _couleur);
	}
}

void Triangle::accept(SavingVisitor* visitor)
{
	{ visitor->save(this); }
}

void Triangle::accept(const DrawingVisitor* visitor)const
{
	{ visitor->draw(this); }
}

ostream & Triangle::afficher(ostream &os) const
{
	os << "TRIANGLE :\n";
	FormeGeo::afficher(os);
	os << " Point A : " << _sommets[0] << "\n Point B : " << _sommets[1] << "\n Point C : " << _sommets[2];
	return os;
}

ostream& operator<<(ostream& s, const Triangle& t)
{
	return t.afficher(s);
}

Triangle::operator string() const
{
	ostringstream os;
	os << "TRIANGLE : \n" << " Couleur : " << FormeGeo::operator std::string() << "\n Point A : " << string(_sommets[0]) << "\n Point B : " << string(_sommets[1]) << "\n Point C : " << string(_sommets[2]) << endl;;
	return os.str();
}

bool Triangle::operator==(const Triangle & t) const
{
	if (FormeGeo::operator==(t) == false)
		return false;
	else
		return (_sommets[0] == t._sommets[0] && _sommets[1] == t._sommets[1] && _sommets[2] == t._sommets[2]);
}