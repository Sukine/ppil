#pragma once
#include "FormeGeo.h"

class Transformation;
class SocketHandler;
class SavingVisitor;
class DrawingVisitor;

class Segment : public FormeGeo
{
	Vecteur2D  _a;
	Vecteur2D  _b;

public:

	Segment(Vecteur2D a, Vecteur2D b, string couleur);
	Segment(const Segment &s);
	~Segment();

	inline const Vecteur2D getA() const { return _a; };
	inline const Vecteur2D getB() const { return _b; };

	virtual const double getAire() const;
	virtual const double getLongueur() const;

	virtual FormeGeo* appliquer(const Transformation* t)  const throw (Erreur);

	virtual void accept(SavingVisitor* visitor);
	virtual void accept(const DrawingVisitor* visitor)const;


	virtual ostream & afficher(ostream & os) const;
	friend ostream&	operator<<(ostream & os, const FormeGeo &f);
	virtual operator string() const;

	bool operator==(Segment const &s) const;
};

