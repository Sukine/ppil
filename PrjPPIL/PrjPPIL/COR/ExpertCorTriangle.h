#pragma once
#include "ExpertCor.h"
#include<vector>

class ExpertCorTriangle : public ExpertCor
{
public:
	ExpertCorTriangle(ExpertCor* sui);
	~ExpertCorTriangle();
	FormeGeo* load1(const vector<string> v, FILE* f) const;
	bool estDetecte(const string typeforme) const;
};