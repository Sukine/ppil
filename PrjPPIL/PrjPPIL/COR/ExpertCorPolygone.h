#pragma once
#include "ExpertCor.h"
#include<vector>

class ExpertCorPolygone : public ExpertCor
{
public:
	ExpertCorPolygone(ExpertCor* sui);
	~ExpertCorPolygone();
	FormeGeo* load1(const vector<string> v, FILE* f) const;
	bool estDetecte(const string typeforme) const;
};