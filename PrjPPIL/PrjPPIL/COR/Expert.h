#pragma once
#include <string>
class FormeGeo;

using namespace std;

class Expert
{
public:
	/**
	@param s : ligne du fichier a analyser
	@return la solution (la formeGeo correspondante) ou NULL en cas d�chec
	*/
	Expert();
	
	virtual  FormeGeo* load(const string& s,FILE *f) const = 0;


};
