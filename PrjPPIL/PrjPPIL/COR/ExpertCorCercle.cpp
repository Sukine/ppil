#include "ExpertCorCercle.h"
#include <iostream>
#include<cstdlib>
#include "../FormesGeometriques/Cercle.h"

using namespace std;

ExpertCorCercle::ExpertCorCercle(ExpertCor* suiv) : ExpertCor(suiv) {}

ExpertCorCercle::~ExpertCorCercle()
{
}

bool ExpertCorCercle::estDetecte(const string typeforme) const
{
	if (typeforme == "C")
		return true;
	return false;
}

FormeGeo* ExpertCorCercle::load1(const vector<string> v, FILE* f) const
{
	double x1 = atoi(v.at(4).c_str());
	double y1 = atoi(v.at(5).c_str());
	double x2 = atoi(v.at(6).c_str());
	double y2 = atoi(v.at(7).c_str());

	Cercle* c = new Cercle(Segment(Vecteur2D(1, 1), Vecteur2D(9, 9), v.at(3)), v.at(1));
}