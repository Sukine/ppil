#pragma once
#include "ExpertCor.h"
#include<vector>

class ExpertCorCercle : public ExpertCor
{
public:
	ExpertCorCercle(ExpertCor* sui);
	~ExpertCorCercle();
	FormeGeo* load1(const vector<string> v, FILE* f) const;
	bool estDetecte(const string typeforme) const;
};