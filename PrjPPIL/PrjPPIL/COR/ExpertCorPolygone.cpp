#include "ExpertCorPolygone.h"
#include <iostream>
#include<cstdlib>
#include"../FormesGeometriques/Polygone.h"

using namespace std;

ExpertCorPolygone::ExpertCorPolygone(ExpertCor* suiv) : ExpertCor(suiv) {}

ExpertCorPolygone::~ExpertCorPolygone()
{
}

bool ExpertCorPolygone::estDetecte(const string typeforme) const
{
	if (typeforme == "P")
		return true;
	return false;
}

FormeGeo* ExpertCorPolygone::load1(const vector<string> v, FILE* f) const
{
	int const size = atoi(v.at(3).c_str());
	vector<Vecteur2D> points;

	for(int i=4; i<size*2+4 ; i = i+2)
	{
		int x = atoi(v.at(i).c_str());
		int y = atoi(v.at(i+1).c_str());
		points.push_back(Vecteur2D(x, y));
	}

	return new Polygone(points, v.at(1));
}