#pragma once
#include"Expert.h"
#include<string>
#include<vector>

using namespace std;


class ExpertCor : public Expert
{
	ExpertCor* _suivant;  // expert suivant dans la cha�ne

public:
	ExpertCor(ExpertCor* expertSuivant);
	~ExpertCor();
	/**
	*algo utilis� : parcourt la cha�ne
	*Convention : en cas d'�chec  retourne NULL
	*/
	FormeGeo* load(const string& d, FILE * f) const;

	/**
	*savoir-faire d'un expert en particulier. D�fini par les classes d�riv�es de ExpertCor
	*Convention : en cas d'�chec  retourne NULL
	*/
	virtual FormeGeo* load1(const vector<string> v, FILE * f) const = 0;

	/**
	* Identifie la bonne forme en fonction de la lettre recu
	*/
	virtual bool estDetecte(const string typeforme) const = 0;

};