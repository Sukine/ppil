#pragma once
#include "ExpertCor.h"
#include<vector>

class ExpertCorSegment : public ExpertCor
{
public :
	ExpertCorSegment(ExpertCor* sui);
	~ExpertCorSegment();
	FormeGeo* load1(const vector<string> v, FILE* f) const;
	bool estDetecte(const string typeforme) const;
};