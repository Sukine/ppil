#include "ExpertCorSegment.h"
#include "../FormesGeometriques/Segment.h"
#include <iostream>
#include<cstdlib>

using namespace std;

ExpertCorSegment::ExpertCorSegment(ExpertCor* suiv) : ExpertCor(suiv){}

ExpertCorSegment::~ExpertCorSegment()
{
}

bool ExpertCorSegment::estDetecte(const string typeforme) const
{
	if (typeforme == "S")
		return true;
	return false;
}

FormeGeo* ExpertCorSegment::load1(const vector<string> v, FILE* f) const
{
	double x1 = atoi(v.at(2).c_str());
	double y1 = atoi(v.at(3).c_str());
	double x2 = atoi(v.at(4).c_str());
	double y2 = atoi(v.at(5).c_str());

	return new Segment(Vecteur2D(x1, y1), Vecteur2D(x2,y2), v.at(1));
}