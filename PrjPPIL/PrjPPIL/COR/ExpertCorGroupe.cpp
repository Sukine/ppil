#include "ExpertCorGroupe.h"
#include <iostream>
#include<cstdlib>
#include"../FormesGeometriques/Groupe.h"
#include "../FormesGeometriques/Segment.h"

using namespace std;

ExpertCorGroupe::ExpertCorGroupe(ExpertCor* suiv) : ExpertCor(suiv) {}

ExpertCorGroupe::~ExpertCorGroupe()
{
}

bool ExpertCorGroupe::estDetecte(const string typeforme) const
{
	if (typeforme == "G")
		return true;
	return false;
}

FormeGeo* ExpertCorGroupe::load1(const vector<string> v, FILE* f) const
{
	char * trame = _strdup("");
	vector<FormeGeo*> formes;
	char* fingrp = _strdup_strdup("FIN_GROUPE\n");

	while ( strcmp (fgets (trame, 500, f),fingrp ) != 0 ) {
		formes.push_back(load((string)trame ,f));
		cout << "On load la forme" << endl;
	}
	free(trame);
	free(fingrp);
	return new Groupe(formes, v.at(2));
}