#include "ExpertCor.h"
#include<vector>
#include<sstream>
#include <iostream>

using namespace std;

ExpertCor::ExpertCor(ExpertCor* expertSuivant) :_suivant(expertSuivant) { }

ExpertCor::~ExpertCor()
{
	free(_suivant);
}

FormeGeo* ExpertCor::load(const string& s, FILE* f) const
{
	//On split la chaine recu en fonction du delimiteur ':' 
	vector<string> vect;
	stringstream ss(s);
	string sousChaine;
	while (getline(ss, sousChaine, ':'))
		vect.push_back(sousChaine);

	if (estDetecte(vect.at(0)))// cet expert a trouv� une solution 
		return this->load1(vect ,f);
	else// �chec de cet expert

		if (this->_suivant != NULL) // le probl�me est transmis � l�expert suivant
			return this->_suivant->load(s, f);

		else    // cet expert est le dernier de la cha�ne donc �chec de la cha�ne
			return NULL;
}