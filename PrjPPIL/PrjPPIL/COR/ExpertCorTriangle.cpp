#include "ExpertCorTriangle.h"
#include <iostream>
#include<cstdlib>
#include"../FormesGeometriques/Triangle.h"

using namespace std;

ExpertCorTriangle::ExpertCorTriangle(ExpertCor* suiv) : ExpertCor(suiv) {}

ExpertCorTriangle::~ExpertCorTriangle()
{
}

bool ExpertCorTriangle::estDetecte(const string typeforme) const
{
	if (typeforme == "T")
		return true;
	return false;
}

FormeGeo* ExpertCorTriangle::load1(const vector<string> v, FILE* f) const
{
	double x1 = atoi(v.at(2).c_str());
	double y1 = atoi(v.at(3).c_str());
	double x2 = atoi(v.at(4).c_str());
	double y2 = atoi(v.at(5).c_str());
	double x3 = atoi(v.at(6).c_str());
	double y3 = atoi(v.at(7).c_str());

	return new Triangle(Vecteur2D(x1,y1), Vecteur2D(x2,y2), Vecteur2D(x3,y3), v.at(1));

}