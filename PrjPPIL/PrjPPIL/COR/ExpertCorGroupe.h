#pragma once
#include "ExpertCor.h"
#include<vector>

class ExpertCorGroupe : public ExpertCor
{
public:
	ExpertCorGroupe(ExpertCor* sui);
	~ExpertCorGroupe();
	FormeGeo* load1(const vector<string> v, FILE* f) const;
	bool estDetecte(const string typeforme) const;
};
