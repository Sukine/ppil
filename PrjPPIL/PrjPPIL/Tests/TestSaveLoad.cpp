#include"../Visitor/SavingVisitor/VisitorSaveTxt.h"
#include "../FormesGeometriques/Vecteur2D.h"
#include "../FormesGeometriques/Segment.h"
#include "../FormesGeometriques/Cercle.h"
#include "../FormesGeometriques/Polygone.h"
#include "../FormesGeometriques/Triangle.h"
#include "../FormesGeometriques/Groupe.h"
#include "../Transformations/Translation.h"
#include "../Transformations/Rotation.h"
#include "../Transformations/Homothétie.h"
#include"../COR/ExpertCorSegment.h"
#include"../COR/ExpertCorCercle.h"
#include"../COR/ExpertCorTriangle.h"
#include"../COR/ExpertCorPolygone.h"
#include"../COR/ExpertCorGroupe.h"



int main()
{
	Vecteur2D v1(1, 2);
	Vecteur2D v2(2, 3);
	Segment* s = new Segment(v1, v2, "red");

	string name = "save.txt"; //le nom du fichier de sauvegarde
	VisitorSaveTxt * SaveTxt = new VisitorSaveTxt(name);

	s->accept(SaveTxt);
	
	Vecteur2D p1(1, 8);
	Vecteur2D p2(4, 10);
	Vecteur2D p3(3, 5);
	Triangle* t = new Triangle(p1, p2, p3, "noir");

	t->accept(SaveTxt);

	Cercle* c = new Cercle(Segment(Vecteur2D(1, 1), Vecteur2D(9, 9), "noir"), "bleu");

	c->accept(SaveTxt);

	Vecteur2D p9(2, 1);
	Vecteur2D p7(2, 2);
	Vecteur2D p8(1, 2);
	Vecteur2D p6(1, 1);
	Polygone* poly = new Polygone("jaune");
	poly->ajouter(p9);
	poly->ajouter(p7);
	poly->ajouter(p8);
	poly->ajouter(p6);

	poly->accept(SaveTxt);


	Groupe* grp = new Groupe("rose");
	try
	{
		grp->ajouter(s);
		grp->ajouter(t);
		grp->ajouter(c);
		grp->ajouter(poly);
	}
	catch (Erreur e)
	{
		cout << e << endl;
	}

	grp->accept(SaveTxt);

	
	ExpertCor* expert = new ExpertCorSegment(NULL);
	expert = new ExpertCorCercle(expert);
	expert = new ExpertCorTriangle(expert);
	expert = new ExpertCorPolygone(expert);
	expert = new ExpertCorGroupe(expert);

	vector<FormeGeo*> resultat;

	FILE* f;
	char ligne[500];
	f = fopen(name.c_str(), "r");
	if (f != NULL)
	{
		while (fgets(ligne, 500, f) != NULL)
		{
			resultat.push_back(expert->load((string)ligne , f)); //On load
			cout << " FORME : " << endl;
		}
		fclose(f);
	}
	else
		cout << "Erreur lors de l'ouverture du fichier" << endl;

	cout << "On afficher les formes qu'on a charge" << endl;
	for (int i = 0; i < resultat.size(); i++)
	{
		if (resultat.at(i) != NULL)
			cout << " FORME " << i+1 << ": " << string(*resultat[i]) << endl;
	}
	
	return 0;
}