#include "../Autres/Erreur.h"
#include "../Autres/Winsock.h"
#include "../Autres/SocketHandler.h"
#include "../FormesGeometriques/Vecteur2D.h"
#include "../FormesGeometriques/Segment.h"
#include "../FormesGeometriques/Cercle.h"
#include "../FormesGeometriques/Polygone.h"
#include "../FormesGeometriques/Triangle.h"
#include "../FormesGeometriques/Groupe.h"
#include "../Transformations/Translation.h"
#include "../Transformations/Rotation.h"
#include "../Transformations/Homoth�tie.h"
#include "../Visitor/DrawingVisitor/VisitorDrawJavaAwt.h"

int main()
{
	////////////////////////////////////////////////////////////////////    INITIALISATION    ////////////////////////////////////////////////////////////////////
	// pointeurs sur l'unique instance de la classe UniqueObject
	Winsock* obj = NULL;

	// initialisation des pointeurs
	obj = Winsock::getInstance();

	//on construit une instance de SocketHandler qui va nous permettre de manipuler les sockets
	SOCKET sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	SOCKADDR_IN sin;
	sin.sin_family = AF_INET;
	sin.sin_port = htons(9111);
	SocketHandler* sh = new SocketHandler(sock, sin);

	sh->initSock();

	//On envoie un premier message : il s'agit des informations pour construire la fen�tre d'affichage c�t� serveur
	const char* msg = _strdup("INITIALISATION\n");
	sh->sendSock(msg, strlen(msg));

	////////////////////////////////////////////////////////////////////    DEBUT DES TESTS    ////////////////////////////////////////////////////////////////////

	Translation* trans = new Translation(Vecteur2D(2, 2));
	Rotation* rota = new Rotation(90, Vecteur2D(0, 0));
	Homoth�tie* homo = new Homoth�tie(2, Vecteur2D(0, 0));

	VisitorDrawJavaAwt * DrawJavaAwt = new VisitorDrawJavaAwt(sh);

	//on commence � envoyer les messages
	//DECOMMENTER dessinerUneForme POUR TESTER

	//SEGMENT
	Segment* s = new Segment(Vecteur2D(0, 0), Vecteur2D(2, 2), "blue");
	s->accept(DrawJavaAwt);
	s = (Segment*)s->appliquer(trans);
	s->accept(DrawJavaAwt);
	s = (Segment*)s->appliquer(rota);
	s->accept(DrawJavaAwt);
	s = (Segment*)s->appliquer(homo);
	s->accept(DrawJavaAwt);
	
	
	//CERCLE
	Cercle* c = new Cercle(*s, "green");
	c->accept(DrawJavaAwt);

	//POLYGONE
	Vecteur2D p9(5, 2);
	Vecteur2D p8(2, 1);
	Vecteur2D p7(0, 5);
	Vecteur2D p6(2, 7);
	Vecteur2D p5(6, 6);

	Polygone* p = new Polygone("red");

	p->ajouter(p9);
	p->ajouter(p8);
	p->ajouter(p7);
	p->ajouter(p6);
	p->ajouter(p5);

	p->accept(DrawJavaAwt);

	
	//TRIANGLE
	Vecteur2D p1(-3, 2);
	Vecteur2D p2(-6, -2);
	Vecteur2D p3(-1, -1);
	Triangle* t = new Triangle(p1, p2, p3, "black");
	t->accept(DrawJavaAwt);


	//GROUPE
	Groupe* grp = new Groupe("pink");
	cout << *grp << endl;

	grp->ajouter(s);
	grp->ajouter(c);
	grp->ajouter(p);

	cout << *grp << endl;
	grp->accept(DrawJavaAwt);

	p->accept(DrawJavaAwt);

	closesocket(sock);

	// destruction de l'instance unique
	obj->kill();

	return 0;
}