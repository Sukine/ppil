#include "../Transformations/Homothétie.h"
#include "../FormesGeometriques/Polygone.h"

int main()
{
	/////////////////////////////////////// HOMOTHETIE ////////////////////////////////////

	Homothétie* h = new Homothétie(2, Vecteur2D(5, 1));
	
	//Homothétie d'un SEGMENT
	cout << "////// HOMOTHETIE D'UN SEGMENT \\\\\\" << endl;

	Segment *sTrans = new Segment(Vecteur2D(4, 2), Vecteur2D(6, 2), "noir");

	cout << "Avant l'homothetie, voici le segment : \n\n" << *sTrans << endl;
	cout << "\nLa longueur du segment : " << sTrans->getLongueur() << endl;
	sTrans = sTrans->appliquer(h);

	cout << "Apres l'homothetie, voici le segment : \n\n" << *sTrans << endl;
	cout << "\nLa longueur du segment : " << sTrans->getLongueur() << endl;


	//Translation d'un CERCLE
	cout << "////// HOMOTHETIE D'UN CERCLE \\\\\\" << endl;

	Cercle *cTrans = new Cercle(Segment(Vecteur2D(5, 3), Vecteur2D(6,3), "noir"), "rouge");
	cout << "Avant l'homothetie :\n\n" << *cTrans << endl;

	cTrans = cTrans->appliquer(h);

	cout << "Apres l'homothetie :\n\n" << *cTrans << endl;

	//Homothétie d'un TRIANGLE
	cout << "////// HOMOTETHIE D'UN TRIANGLE \\\\\\" << endl;

	Triangle * tTrans = new Triangle(Vecteur2D(4, 2), Vecteur2D(5, 4), Vecteur2D(6, 2), "noir");
	cout << "Avant l'homotéthie :\n\n" << *tTrans << endl;

	tTrans = tTrans->appliquer(h);

	cout << "Apres l'homotéthie :\n\n" << *tTrans << endl;

	//Homothétie d'un POLYGONE
	cout << "////// TRANSLATION D'UN POLYGONE \\\\\\" << endl;

	Vecteur2D p9(4, 4);
	Vecteur2D p7(6, 4);
	Vecteur2D p8(6, 2);
	Vecteur2D p6(4, 2);

	vector<Vecteur2D> _sommets;
	_sommets.push_back(p9);
	_sommets.push_back(p7);
	_sommets.push_back(p8);
	_sommets.push_back(p6);

	Polygone * pTrans = new Polygone(_sommets, "noir");
	cout << "Avant l'homotéthie :\n\n" << *pTrans << endl;

	pTrans = pTrans->appliquer(h);

	cout << "Apres l'homothétie :\n\n" << *pTrans << endl;

	//Homothétie d'un GROUPE
	cout << "////// HOMOTHETIE D'UN GROUPE \\\\\\" << endl;

	Groupe * grpTrans = new Groupe("rose");

	grpTrans->ajouter(pTrans);
	grpTrans->ajouter(tTrans);

	cout << "\nAvant l'homothétie \n\n" << endl;
	cout << *grpTrans << endl;
	cout << "\nL'aire du groupe  : " << grpTrans->getAire() << "\n" << endl;

	grpTrans = grpTrans->appliquer(h);

	cout << "\nApres l'homothétie \n\n" << endl;
	cout << *grpTrans << endl;
	cout << "\nL'aire du groupe  : " << grpTrans->getAire() << "\n" << endl;
}