#include "../Autres/Erreur.h"
#include "../Autres/Winsock.h"
#include "../Autres/SocketHandler.h"
#include "../FormesGeometriques/Vecteur2D.h"
#include "../FormesGeometriques/Segment.h"
#include "../FormesGeometriques/Cercle.h"
#include "../FormesGeometriques/Polygone.h"
#include "../FormesGeometriques/Triangle.h"
#include "../FormesGeometriques/Groupe.h"
#include "../Transformations/Translation.h"
#include "../Transformations/Rotation.h"
#include "../Transformations/Homothétie.h"
#include <iostream>

int main() {
	
	//////////////////////////////////////////////////////// INITIALISATION DES FORMES ////////////////////////////////////////////////////////

	Vecteur2D p1(1, 8);
	Vecteur2D p2(4, 10);
	Vecteur2D p3(3, 5);

	Triangle* t = new Triangle(p1, p2, p3, "rouge");

	Segment* s = new Segment(p1, p2, "rouge");

	Vecteur2D p9(2, 1);
	Vecteur2D p7(2, 2);
	Vecteur2D p8(1, 2);
	Vecteur2D p6(1, 1);
	Polygone* poly = new Polygone("jaune");
	poly->ajouter(p9);
	poly->ajouter(p7);
	poly->ajouter(p8);
	poly->ajouter(p6);

	Cercle* c = new Cercle(Segment(Vecteur2D(1, 1), Vecteur2D(9, 9), "noir"), "bleu");

	Groupe* grp = new Groupe("rose");
	grp->ajouter(s);
	grp->ajouter(t);
	grp->ajouter(c);
	grp->ajouter(poly);

	/////////////////////////////////////////////////////DECOMMENTER LE CONTENU DES TRY POUR TESTER/////////////////////////////////////////////////////

	//DESSINERUNEFORME
	SocketHandler *h = NULL;

	try {
		//t->dessinerUneForme(h);
		//s->dessinerUneForme(h);
		//poly->dessinerUneForme(h);
		//c->dessinerUneForme(h);
		//grp->dessinerUneForme(h);
	}
	catch (Erreur e) {
		cerr << e << endl;
	}

	//APPLIQUER
	Rotation* rt = NULL;

	try {
		//t = t->appliquer(rt);
		//s = s->appliquer(rt);
		//poly->appliquer(rt);
		//c->appliquer(rt);
		//grp->appliquer(rt);
	}
	catch (Erreur e) {
		cerr << e << endl;
	}

	//SENDSOCK
	Winsock* obj = NULL;
	obj = Winsock::getInstance();

	SOCKET sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	SOCKADDR_IN sin;
	sin.sin_family = AF_INET;
	sin.sin_port = htons(9111);
	SocketHandler* sh = new SocketHandler(sock, sin);

	sh->initSock(); 

	const char* tr = _strdup(s->getTrame().c_str());

	try {
		//sh->sendSock(tr, 0);
		//sh->sendSock(tr, -1);
		//sh->sendSock("", 3);
		//sh->sendSock("", 0);
	}
	catch (Erreur e) {
		cerr << e << endl;
	}

	closesocket(sock);
	obj->kill();

	//CONST POLYGONE
	vector<Vecteur2D> sommets;
	try {
		//Polygone * p2 = new Polygone(sommets, "vert");
	}
	catch (Erreur e) {
		cerr << e << endl;
	}
	
	//CONST GROUPE
	vector<FormeGeo*> formes;
	try {
		//Groupe* g = new Groupe(formes, "vert");
	}
	catch (Erreur e) {
		cerr << e << endl;
	}
	
	//CONST FORMEGEO
	try {
		//Segment* s2 = new Segment(p3, p2, "");
	}
	catch (Erreur e) {
		cerr << e << endl;
	}

	return 0;
}