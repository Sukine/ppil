#include "../Transformations/Translation.h"
#include "../FormesGeometriques/Polygone.h"

int main()
{
	/////////////////////////////////////// TRANSLATION ////////////////////////////////////
	Translation * t = new Translation(Vecteur2D(1, 2));
	
	//Translation d'un SEGMENT
	cout << "////// TRANSLATION D'UN SEGMENT \\\\\\" << endl;

	Segment *sTrans = new Segment(Vecteur2D(1, 1), Vecteur2D(3, 1), "noir");

	cout << "Avant la translation, voici le segment : \n\n" << *sTrans << endl;
	cout << "\nLa longueur du segment : " << sTrans->getLongueur() << endl;
	sTrans = sTrans->appliquer(t);

	cout << "Apres la translation, voici le segment : \n\n" << *sTrans << endl;
	cout << "\nLa longueur du segment : " << sTrans->getLongueur() << endl;


	//Translation d'un CERCLE
	cout << "////// TRANSLATION D'UN CERCLE \\\\\\" << endl;

	Cercle *cTrans = new Cercle(Segment(Vecteur2D(0, 0), Vecteur2D(2,0),"noir"), "rouge");
	cout << "Avant la translation :\n\n" << *cTrans << endl;

	cTrans = cTrans->appliquer(t);

	cout << "Apres la translation :\n\n" << *cTrans << endl;

	//Translation d'un TRIANGLE
	cout << "////// TRANSLATION D'UN TRIANGLE \\\\\\" << endl;

	Triangle * tTrans = new Triangle(Vecteur2D(0, 0), Vecteur2D(0, 1), Vecteur2D(2, 0), "noir");
	cout << "Avant la translation :\n\n" << *tTrans << endl;

	tTrans = tTrans->appliquer(t);

	cout << "Apres la translation :\n\n" << *tTrans << endl;

	//Translation d'un POLYGONE
	cout << "////// TRANSLATION D'UN POLYGONE \\\\\\" << endl;
/*
	Vecteur2D p9(2, 1);
	Vecteur2D p7(2, 2);
	Vecteur2D p8(1, 2);
	Vecteur2D p6(1, 1);

	vector<Vecteur2D> _sommets;
	_sommets.push_back(p9);
	_sommets.push_back(p7);
	_sommets.push_back(p8);
	_sommets.push_back(p6);

	Polygone * pTrans = new Polygone(_sommets, "noir");
	cout << "Avant la translation :\n\n" << *pTrans << endl;

	pTrans = pTrans->appliquer(t);

	cout << "Apres la translation :\n\n" << *pTrans << endl;

	//Translation d'un GROUPE
	cout << "////// TRANSLATION D'UN GROUPE \\\\\\" << endl;

	Groupe * grpTrans = new Groupe("rose");

	grpTrans->ajouter(pTrans);
	grpTrans->ajouter(tTrans);

	cout << "\nAvant la translation \n\n" << endl;
	cout << *grpTrans << endl;
	cout << "\nL'aire du groupe  : " << grpTrans->getAire() << "\n" << endl;

	grpTrans = grpTrans->appliquer(t);

	cout << "\nApres la translation \n\n" << endl;
	cout << *grpTrans << endl;
	cout << "\nL'aire du groupe  : " << grpTrans->getAire() << "\n" << endl;*/
	return 0;
}