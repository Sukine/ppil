package expert;

import java.awt.Color;
import java.util.HashMap;

import ui.FenetreDessin;

public class ExpertTriangle extends ExpertCOR{

	public ExpertTriangle(ExpertCOR svt) {
		super(svt);
	}
	
	@Override
	public boolean estDetecte(String typeforme) {
		if (typeforme.equals("T")) return true;
		return false;
	}
	
	@Override
	public void dessiner(String[] split, FenetreDessin fn,HashMap<String, Color> map) {
		int x[] = {CoordToPixels(Double.parseDouble(split[3])),CoordToPixels(Double.parseDouble(split[6])),CoordToPixels(Double.parseDouble(split[9])),CoordToPixels(Double.parseDouble(split[3]))};
		int y[] = {CoordToPixels(Double.parseDouble(split[4])),CoordToPixels(Double.parseDouble(split[7])),CoordToPixels(Double.parseDouble(split[9])),CoordToPixels(Double.parseDouble(split[4]))};
		
		fn.graphics.setColor(map.get(split[1]));
		fn.graphics.drawPolygon(x, y, x.length);
		
	}
}
