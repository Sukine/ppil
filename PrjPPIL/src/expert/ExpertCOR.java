package expert;

import java.awt.Color;
import java.util.HashMap;

import ui.FenetreDessin;

public abstract class ExpertCOR extends Expert{

	public ExpertCOR suivant;
	public final static int UNITE_ECRAN = 25;
	
	protected ExpertCOR(ExpertCOR svt) {
		suivant = svt;
	}
	
	/**
	 * * Identifie la bonne forme
	@param typeforme : correspond � la premi�re lettre du nom de la forme (T pour Triangle par exemple)
	@return vrai si on tombe sur le bon d�tecteur, faux sinon
	*/
	public abstract boolean estDetecte(String typeforme);

	/**
	 * Transforme les coordonn�es rep�re monde en rep�re �cran.
	 * @param x
	 * @return on retourne un entier car les fonctions draw le demande
	 */
	public int CoordToPixels(double x)
	{
		//On dit que 1 unit� dans le monde correspondant � UNITE_ECRAN	px dans l'�cran
		return (int) (UNITE_ECRAN*x);
	}
	
	/**
	 * * Trouve le bon chainon de la COR en fonction du type de forme envoy�
	@param split : string contenant tous les param�tres n�cessaires (couleur, coordonn�es, etc)
	@return false si aucun chainon ne correspond � la forme envoy�e.
	*/
	public boolean resoudre(String msg, FenetreDessin fn,HashMap<String, Color> map) {
		String[] split = msg.split(":");
		if (estDetecte(split[0])) {
				dessiner(split, fn,map);
				return true;
		}
		else   {         			
			if  (suivant != null)  		
				return suivant.resoudre(msg, fn,map);	
			else    
				return false;
		}
	}
	
	/**
	 * * Dessine sur l'interface graphique la forme envoy�e 
	@param split : string contenant tous les param�tres n�cessaires (couleur, coordonn�es, etc)
	@param fn : notre fenetre de dessin
	@param map : une hashmap qui nous aide permet de colorer les formes
	*/
	public abstract void dessiner(String[] split, FenetreDessin fn, HashMap<String, Color> map);
	
	public void setSuivant(ExpertCOR suivant) {
		this.suivant = suivant;
	}
	
}
