package expert;

import java.awt.Color;
import java.util.HashMap;

import ui.FenetreDessin;

public class ExpertGroupe extends ExpertCOR{
	public ExpertGroupe(ExpertCOR svt) {
		super(svt);
	}

	@Override
	public boolean estDetecte(String typeforme) {
		if (typeforme.equals("G")) return true;
		return false;
	}

	@Override
	public void dessiner(String[] split, FenetreDessin fn,HashMap<String, Color> map) {

		String trame="";
		for (int i=2 ; i<split.length;i++)
			trame += split[i] + ":";
		
		//On split notre chaine avec '#' comme delimiteur
		String[] tab = trame.split("#");
		
		//On doit maintenant dessiner les formes une par une, or chacune des trames est stock�e dans tab ...
		for(int j=1;j<tab.length-1;j++)
			resoudre(tab[j],fn,map);
	}

}
