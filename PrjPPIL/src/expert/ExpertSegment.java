package expert;


import java.awt.Color;
import java.util.HashMap;

import ui.FenetreDessin;

public class ExpertSegment extends ExpertCOR {

	public ExpertSegment(ExpertCOR svt) {
		super(svt);
	}

	@Override
	public boolean estDetecte(String typeforme) {
		if (typeforme.equals("S")) return true;
		return false;
	}

	@Override
	public void dessiner(String[] split, FenetreDessin fn,HashMap<String, Color> map) {
		fn.graphics.setColor(map.get(split[1]));
		//On est oblige de caster de cette maniere a cause des transformations
		
		int x1 = CoordToPixels(Double.parseDouble(split[2]));
		int y1 = CoordToPixels(Double.parseDouble(split[3]));
		int x2 = CoordToPixels(Double.parseDouble(split[4]));
		int y2 = CoordToPixels(Double.parseDouble(split[5]));
		
		fn.graphics.drawLine(x1, y1,x2,y2);
	}

}
