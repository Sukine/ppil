package expert;

import java.awt.Color;
import java.util.HashMap;

import ui.FenetreDessin;

public class ExpertCercle extends ExpertCOR{

	public ExpertCercle(ExpertCOR svt) {
		super(svt);
	}

	@Override
	public boolean estDetecte(String typeforme) {
		if (typeforme.equals("C")) return true;
		return false;
	}

	@Override
	public void dessiner(String[] split, FenetreDessin fn,HashMap<String, Color> map) {
		fn.graphics.setColor(map.get(split[1]));

		int rayon = CoordToPixels(Double.parseDouble(split[9]));
		int x = CoordToPixels(Double.parseDouble(split[4]));
		int y = CoordToPixels(Double.parseDouble(split[5]));
		
		//drawOval(centre.x - rayon, centre.y - rayon, 2*rayon, 2*rayon);
		fn.graphics.drawOval(x-rayon, y-rayon, 2*rayon, 2*rayon);
	}

}
