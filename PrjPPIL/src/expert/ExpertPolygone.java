package expert;

import java.awt.Color;
import java.util.HashMap;

import ui.FenetreDessin;

public class ExpertPolygone extends ExpertCOR{

	public ExpertPolygone(ExpertCOR svt) {
		super(svt);
	}

	@Override
	public boolean estDetecte(String typeforme) {
		if (typeforme.equals("P")) return true;
		return false;
	}

	@Override
	public void dessiner(String[] split, FenetreDessin fn,HashMap<String, Color> map) {
		int size = (int)Double.parseDouble(split[3]);
		int x[] = new int[size+1];
		int y[] = new int[size+1];
		int j=0;
		for(int i=4;i<size*2+4;i++)
		{
			if(i%2==0)
				x[j] = CoordToPixels(Double.parseDouble(split[i]));
			else
			{
				y[j] = CoordToPixels(Double.parseDouble(split[i]));
				j++;
			}
		}
		//On doit rajouter une deuxieme fois le premier point du polygone pour fermer la figure
		x[j] = CoordToPixels(Double.parseDouble(split[4]));
		y[j] = CoordToPixels(Double.parseDouble(split[5]));
		fn.graphics.setColor(map.get(split[1]));
		fn.graphics.drawPolygon(x, y, size);
	}

}
