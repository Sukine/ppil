package expert;

import java.awt.Color;
import java.util.HashMap;

import ui.FenetreDessin;

public abstract class Expert {
	/**
	@param split : string contenant tous les paramètres nécessaires (couleur, coordonnées, etc)
	@param fn : notre fenetre de dessin
	@param map : une hashmap qui nous aide permet de colorer les formes
	*/
	public abstract boolean resoudre(String msg, FenetreDessin fn,HashMap<String, Color> map);

	public abstract void setSuivant(ExpertCOR expCercle);

}





