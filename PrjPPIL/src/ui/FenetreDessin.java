package ui;
import java.awt.*;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;


public class FenetreDessin extends JFrame{
	
	public Canvas canvas;
	public Graphics graphics; 
	public BufferedImage img = null;

	
	 public FenetreDessin(int largeur, int hauteur, int ox, int oy) throws InterruptedException {  		
		 
		setLayout(null);
		
		Toolkit tk = Toolkit.getDefaultToolkit();
		
		int le, he; // largeur �cran, hauteur �cran
		
		Dimension dim = tk.getScreenSize(); // dimensions de l'�cran
		
		le = (int) dim.getWidth();
		he = (int) dim.getHeight();
		
		int bordGauche, bordSup�rieur, l, h ;
		
		bordGauche = Math.max(0, ox);
		bordSup�rieur = Math.max(0, oy);
		l = Math.min(largeur, le - bordGauche);
		h = Math.min(hauteur, he - bordSup�rieur);
		setBounds(bordGauche, bordSup�rieur, l, h);
		setLocationRelativeTo(null);
		//this.setBounds(ox, oy, largeur, hauteur);
		
		canvas = new Canvas();
		canvas.setIgnoreRepaint(true);
		canvas.setSize(new Dimension(largeur, hauteur));
		add(canvas);
		
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		/*setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setIgnoreRepaint(true); //On d�sactive la m�thode paint
		setLocationRelativeTo(null);*/
		
		
		canvas.createBufferStrategy(1);
		Thread.sleep(50);  
		graphics = canvas.getBufferStrategy().getDrawGraphics();
		
		//On remet l'origine au centre du canvas
		Graphics2D g2d = (Graphics2D)graphics;
	    g2d.translate(largeur/2, hauteur/2);
	    g2d.scale(1.0, -1.0); //On remet � l'�chelle
	}

	
}
