package serveur;
/**
 * Serveur dessinant des sc�nes 2D en Active-Rendering sur l'�cran local. Les ordres de trac�s sont donn�s par les clients distants.
 * 
 *  Serveur multi-client
 * 
 * 
 * 
 * */

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TestServeurDessin
{
	
	public static void main(String[] args)
	{
		try
		    {
		    ServerSocket svrDessin = new ServerSocket(9111);
		    System.out.println("Serveur pr�t, \n d�tails : "+svrDessin);
		    
		    int nbClients = 0;
		    
		    while (true)
		        {
		        System.out.println("serveur de dessin en attente du prochain client");
		        Socket socket = svrDessin.accept();
		        System.out.println("Clien n� = " + nbClients);
		        ReceveurDessin rcv = new ReceveurDessin(socket);
		        rcv.start();
		        }
		    }
		catch (IOException e)
		    {
		    
		    System.err.println("Le serveur n'a pu �tre connect� sur le port d'�coute ou la connexion sur ce port a �t� rompue. \n D�tails : "+e);
		    }
	}
}
