package serveur;
import java.net.*;
import java.util.HashMap;

import expert.*;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import ui.FenetreDessin;

public class ReceveurDessin extends Thread {
	Socket socket;
	BufferedReader fluxEntrant;
	
	public ReceveurDessin(Socket socket) throws IOException
	{
		this.socket = socket;
		this.fluxEntrant = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
	}
	

	public void run()
	{
		String msg;	
		FenetreDessin fnDessin = null;
		
		Expert  cor;
		ExpertCOR expertSeg,expertCer,expertPoly,expertTrian,expertGroupe;
		expertSeg = new ExpertSegment(null);
		expertCer = new ExpertCercle(expertSeg);
		expertPoly= new ExpertPolygone(expertCer);
		expertTrian= new ExpertTriangle(expertPoly);
		expertGroupe = new ExpertGroupe(expertTrian);
		cor = expertGroupe;	
	
		try
		{
			 HashMap<String,Color> map = new HashMap <String,Color> ();
			 
			 //On initialise ici une HashMap qui va nous permettre de faire la transformation entre un String et une Color
			 map.put("black",Color.BLACK);
			 map.put("red", Color.RED);
			 map.put("green", Color.GREEN);
			 map.put("blue", Color.BLUE);
			 map.put("yellow", Color.YELLOW);
			 map.put("pink", Color.PINK);
			 //etc... on peut en ajouter autant que l'on veut
			 
			 while ((msg = fluxEntrant.readLine()) != null)
			 {
			    System.out.println("Message re�u = " + msg);
				if (!msg.equals("INITIALISATION")) {
					fnDessin = new FenetreDessin(700,700,0,0);
					cor.resoudre(msg, fnDessin, map);
				    fnDessin.canvas.getBufferStrategy().show();
				}
			 }
			
		}
		catch (SocketException e)
	    {
	  	 System.out.println("Session de dessin termin�e par le client");
	    }
		catch (NumberFormatException e)
	    {
			e.printStackTrace();
	    }
		catch (IOException e)
	    {
			e.printStackTrace();
	    }
		catch (InterruptedException e)
		    {
		    e.printStackTrace();
		    }
	}
		}// run
 // ReceveurEnvoyeur

